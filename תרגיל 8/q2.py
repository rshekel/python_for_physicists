from string import ascii_letters 

long_string = input("Please enter long string: ")

d = {}

# Inserting zero's and not using d.getdefault(letter, 0) later on 
# because we want to print at the end also letters with 0 occurences. 
for l in ascii_letters:
    d[l] = 0

for letter in long_string:
    if letter in ascii_letters:
        d[letter] = d[letter] + 1

for k in sorted(d):
    print(k, "\t", d[k])

"""
C:\code\python_for_physicists\תרגיל 8>q2.py
Please enter long string: Some things in life are bad They can really make you mad Other things just make you swear and curse When you're chewing on life's gristle Don't grumble, give a whistle And this'll help things turn out for the best And Always look on the bright side Of life Always look on the light side Of life
A        4
B        0
C        0
D        1
E        0
F        0
G        0
H        0
I        0
J        0
K        0
L        0
M        0
N        0
O        3
P        0
Q        0
R        0
S        1
T        1
U        0
V        0
W        1
X        0
Y        0
Z        0
a        12
b        4
c        3
d        7
e        27
f        7
g        9
h        15
i        17
j        1
k        4
l        17
m        5
n        15
o        14
p        1
q        0
r        11
s        15
t        17
u        8
v        1
w        5
x        0
y        7
z        0
"""