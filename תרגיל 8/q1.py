from collections import Hashable 

l = eval(input("Please enter list of things: "))

hashables = []
non_hashables = []

for k in l:
    if isinstance(k, Hashable):
        hashables.append(k)
    else:
        non_hashables.append(k)

size = min(len(hashables), len(non_hashables))

d = {}
for i in range(size):
    d[hashables[i]] = non_hashables[i]

for k, v in d.items():
    print(k, '\t', v)

"""
C:\code\python_for_physicists\תרגיל 8>q1.py
Please enter list of things: [1,2,[22],'a','b',[33,'ab'],(44,23),[55],{23,'vv'},range(1,10),['dd'],[123],'aa',456]
1        [22]
2        [33, 'ab']
a        [55]
b        {'vv', 23}
(44, 23)         ['dd']
range(1, 10)     [123]
"""
