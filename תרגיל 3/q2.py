num = input("Please enter natural number: ")
digits_sum = 0
for n in num:
    digits_sum += int(n)
print("Sum is: %d" % digits_sum)

"""
C:\code\python_for_physicists\תרגיל 3>q2.py
Please enter natural number: 12345
Sum is: 15
"""
