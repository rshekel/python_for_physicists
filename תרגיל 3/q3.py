import random 
amount = int(input("Please enter even number between 4 and 20: "))
random_nums = []
for i in range(amount):
    random_nums.append(random.randint(1, 100))
print(random_nums)
random_nums.sort()
index = int(len(random_nums) / 2)
print("Middle numbers: %d, %d" % (random_nums[index - 1], random_nums[index]))

"""
C:\code\python_for_physicists\תרגיל 3>q3.py
Please enter even number between 4 and 20: 8
[19, 41, 8, 5, 74, 48, 25, 32]
Middle numbers: 25, 32
"""
