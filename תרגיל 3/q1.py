num = 1
to_print = []
for i in range(1, 10):
    to_print.append("%d * 8 + %d = %d" % (num, i, (num * 8) + i))
    num = num*10 + i + 1
print("\r\n".join(to_print))

"""
C:\code\python_for_physicists\תרגיל 3>q1.py
1 * 8 + 1 = 9
12 * 8 + 2 = 98
123 * 8 + 3 = 987
1234 * 8 + 4 = 9876
12345 * 8 + 5 = 98765
123456 * 8 + 6 = 987654
1234567 * 8 + 7 = 9876543
12345678 * 8 + 8 = 98765432
123456789 * 8 + 9 = 987654321
"""
