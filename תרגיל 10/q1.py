import math 

def is_prime(n):
    assert isinstance(n, int), "must get int parameter"
    if n == 1:
        return False
    max_div = int(math.sqrt(n)) + 1
    for i in range(2, max_div):
        if n % i == 0:
            return False
    return True 

def goldbach(n):
    assert n % 2 == 0, "must get even parameter"
    assert n > 2, "must get parameter bigger than 2"
    for i in range(n):
        if is_prime(i):
            if is_prime(n - i):
                print(i, n-i, "are primes that sum to", n)
                return 

a, b = input("Please enter two numbers bigger than 2, divided by \",\":").split(',')
a = int(a)
b = int(b)

for i in range(min(a, b), max(a, b)):
    if i % 2 == 0:
        goldbach(i)

"""
C:\code\python_for_physicists\תרגיל 10>q1.py
Please enter two numbers bigger than 2, divided by ",":2000,2100
3 1997 are primes that sum to 2000
3 1999 are primes that sum to 2002
5 1999 are primes that sum to 2004
3 2003 are primes that sum to 2006
5 2003 are primes that sum to 2008
7 2003 are primes that sum to 2010
13 1999 are primes that sum to 2012
3 2011 are primes that sum to 2014
5 2011 are primes that sum to 2016
7 2011 are primes that sum to 2018
3 2017 are primes that sum to 2020
5 2017 are primes that sum to 2022
7 2017 are primes that sum to 2024
23 2003 are primes that sum to 2026
11 2017 are primes that sum to 2028
3 2027 are primes that sum to 2030
3 2029 are primes that sum to 2032
5 2029 are primes that sum to 2034
7 2029 are primes that sum to 2036
11 2027 are primes that sum to 2038
11 2029 are primes that sum to 2040
3 2039 are primes that sum to 2042
5 2039 are primes that sum to 2044
7 2039 are primes that sum to 2046
19 2029 are primes that sum to 2048
11 2039 are primes that sum to 2050
13 2039 are primes that sum to 2052
37 2017 are primes that sum to 2054
3 2053 are primes that sum to 2056
5 2053 are primes that sum to 2058
7 2053 are primes that sum to 2060
23 2039 are primes that sum to 2062
11 2053 are primes that sum to 2064
3 2063 are primes that sum to 2066
5 2063 are primes that sum to 2068
7 2063 are primes that sum to 2070
3 2069 are primes that sum to 2072
5 2069 are primes that sum to 2074
7 2069 are primes that sum to 2076
61 2017 are primes that sum to 2078
11 2069 are primes that sum to 2080
13 2069 are primes that sum to 2082
3 2081 are primes that sum to 2084
3 2083 are primes that sum to 2086
5 2083 are primes that sum to 2088
3 2087 are primes that sum to 2090
3 2089 are primes that sum to 2092
5 2089 are primes that sum to 2094
7 2089 are primes that sum to 2096
11 2087 are primes that sum to 2098
"""