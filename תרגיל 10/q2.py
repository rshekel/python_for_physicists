def getPentaNum(n):
    return int(n*(3*n - 1) / 2)
    
def test_penta():
    print("First hundred penta numbers:")
    for i in range(1, 100):
        print(getPentaNum(i), end=', ')
        if i % 10 == 0:
            print()
    print()
        
def sumDigits(n):
    sum = 0
    while n > 0:
        sum += n % 10
        n = int(n / 10)
    return sum

def test_sum_digits():
    n = int(input("Please enter number: "))
    print("Sum of digits of your number is: ", sumDigits(n))

def reverse(n):
    s = str(n)
    return int(s[::-1])

def test_reverse():
    n = int(input("Please enter num: "))
    print("Your number reversed is: ", reverse(n))

test_penta()
test_sum_digits()
test_reverse()

"""
C:\code\python_for_physicists\תרגיל 10>q2.py
First hundred penta numbers:
1, 5, 12, 22, 35, 51, 70, 92, 117, 145,
176, 210, 247, 287, 330, 376, 425, 477, 532, 590,
651, 715, 782, 852, 925, 1001, 1080, 1162, 1247, 1335,
1426, 1520, 1617, 1717, 1820, 1926, 2035, 2147, 2262, 2380,
2501, 2625, 2752, 2882, 3015, 3151, 3290, 3432, 3577, 3725,
3876, 4030, 4187, 4347, 4510, 4676, 4845, 5017, 5192, 5370,
5551, 5735, 5922, 6112, 6305, 6501, 6700, 6902, 7107, 7315,
7526, 7740, 7957, 8177, 8400, 8626, 8855, 9087, 9322, 9560,
9801, 10045, 10292, 10542, 10795, 11051, 11310, 11572, 11837, 12105,
12376, 12650, 12927, 13207, 13490, 13776, 14065, 14357, 14652,
Please enter number: 12345
Sum of digits of your number is:  15
Please enter num: 12345
Your number reversed is:  54321
"""
