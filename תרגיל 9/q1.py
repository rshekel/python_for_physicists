import math 

def get_dervs_sum(x):
    sum = 1
    upper_limit = int(math.sqrt(x)) + 1
    for i in range(2, upper_limit):
        if x % i == 0:
            sum += i 
            sum += x / i
    return int(sum)

lovers = []
num = 1

while True:
    possible_lover = get_dervs_sum(num)
    if get_dervs_sum(possible_lover) == num and possible_lover != num and num not in lovers:
        lovers += [num, possible_lover]
        print("The numbers: ", num, ",", possible_lover, "are lovers!")
        letter = input("Should i keep on searching for lover numbers? (y for yes, other for no) ")
        if letter != 'y':
            print("Goodbye!")
            break
    num += 1 
    
"""
C:\code\python_for_physicists\תרגיל 9>q1.py
The numbers:  220 , 284 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  1184 , 1210 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  2620 , 2924 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  5020 , 5564 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  6232 , 6368 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  10744 , 10856 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  12285 , 14595 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  17296 , 18416 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  63020 , 76084 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no) y
The numbers:  66928 , 66992 are lovers!
Should i keep on searching for lover numbers? (y for yes, other for no)
Goodbye!
"""
