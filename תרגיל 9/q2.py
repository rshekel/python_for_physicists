def same_vowels(s1, s2):
    # We care about vowels disregarding their case 
    s1 = s1.lower()
    s2 = s2.lower()
    for v in ['a', 'e', 'i', 'o', 'u']:
        if s1.count(v) != s2.count(v):
            return False
    return True 

"""
In [42]: paste
def same_vowels(s1, s2):
    # We care about vowels disregarding their case
    s1 = s1.lower()
    s2 = s2.lower()
    for v in ['a', 'e', 'i', 'o', 'u']:
        if s1.count(v) != s2.count(v):
            return False
    return True
## -- End pasted text --

In [43]: same_vowels('aabcefiok','xcexvcxaioa')
Out[43]: True

In [44]: same_vowels('aabcefiok','xcexvcxaioia')
Out[44]: False
"""
