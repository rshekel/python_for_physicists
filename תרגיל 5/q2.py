import math 
AMOUNT = 3

count = 0
perfects = []
num = 2

while True:
    divisors = [1]
    for i in range(2, round(math.sqrt(num)) + 1):
        if num % (i) == 0:
            divisors.append(i)
            if i != num / i:
                divisors.append(int(num / i))

    if sum(divisors) == num:
        perfects.append(num)
    if len(perfects) == AMOUNT:
        break 
        
    num += 1 

print("Perfects: ", perfects)

"""
C:\code\python_for_physicists\תרגיל 5>q2.py
Perfects:  [6, 28, 496]
"""
