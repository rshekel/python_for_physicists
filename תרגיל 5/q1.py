import math 

num = int(input("Please enter number larger than 2: "))

assert num > 2

if num % 2 == 0:
    print("no")
else:
    is_prime = True 
    for q in range(3, int(math.sqrt(num)) + 1, 2):
        if num % q == 0:
            is_prime = False
            break
    print("yes" if is_prime else "no")

"""
C:\code\python_for_physicists\תרגיל 5>q1.py
Please enter number larger than 2: 1001
no

C:\code\python_for_physicists\תרגיל 5>q1.py
Please enter number larger than 2: 1003
no

C:\code\python_for_physicists\תרגיל 5>q1.py
Please enter number larger than 2: 1007
no

C:\code\python_for_physicists\תרגיל 5>q1.py
Please enter number larger than 2: 1009
yes
"""
