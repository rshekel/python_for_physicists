l = eval(input("Please enter list of visits: "))

friend_visits = []
friends = []
dates = []

for name, date in l:
    if name not in friends:
        friends.append(name)
    friend_visits.append(name)
    dates.append(date)
    
for friend in friends:
    print(friend, "visited me", friend_visits.count(friend), "times this year!")

first_third = 0
second_third = 0
third_third = 0
for date in dates:
    day = date[0]
    if 1 <= day <= 10:
        first_third += 1 
    elif 11 <= day <= 20:
        second_third += 1 
    else:
        third_third += 1 

most_visited = max(first_third, second_third, third_third)

best_third = 'first' 
if most_visited == second_third:
    best = 'second'
elif most_visited == third_third:
    best = 'third'
print("The", best, "part of the month was the one with most visits!")

"""
C:\code\python_for_physicists\תרגיל 6>q2.py
Please enter list of visits: [["moshe",(11,3,2017)],["yair",(22,11,1017)],["haim",(10,6,2017)],["hilel",(3,4,2017)],["moshe",(11,4,2017)],["hilel",(23,8,2017)],["moshe",(22,9,2017)],["moshe",(31,1,2017)]]
moshe visited me 4 times this year!
yair visited me 1 times this year!
haim visited me 1 times this year!
hilel visited me 2 times this year!
The third part of the month was the one with most visits!
"""
