import math 

my_num = int(input("Enter number: "))

# keeping initial zero for easy 0_indexing issues 
l = list(range(my_num + 1))

for num in range(2, int(math.sqrt(my_num) + 1)):    
    if l[num] == 0:
        continue
    index = num * 2

    while True:
        if index <= (len(l) -1):
            l[index] = 0
            index += num
        else:
            break

primes = []
for i in l:
    if i != 0 and i != 1:
        primes.append(i)
        
print(primes)
print(len(primes), "primes")

"""
C:\code\python_for_physicists\תרגיל 5>q3.py
Enter number: 100
[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
25 primes
"""
