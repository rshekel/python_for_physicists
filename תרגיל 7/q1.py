import string 
import sys 

letters = string.ascii_letters + " "

key = input("Please enter key: ")
for l in key:
    if l not in letters:
        print("Key can contain only ascii_letters or space!")
        sys.exit(1)

note = input("Please enter note: ")
for l in note:
    if l not in letters:
        print("note can contain only ascii_letters or space!")
        sys.exit(1)

i = int(input("For encryption enter 1, for decryption enter 2: "))
if i not in (1, 2):
    print("Invalid choice!")
    sys.exit(1)

enc = True if i == 1 else False

new_str = ""
split = 0

if enc:
    for l in note:
        index = letters.index(l)
        relevant_letter_in_key = key[split % len(key)]
        key_letter_index = letters.index(relevant_letter_in_key)
        new_index = (index + key_letter_index) % len(letters)
        new_str += letters[new_index]
        split += 1
    print(new_str)
else:
    for l in note:
        index = letters.index(l)
        relevant_letter_in_key = key[split % len(key)]
        key_letter_index = letters.index(relevant_letter_in_key)
        new_index = (index - key_letter_index) % len(letters)
        new_str += letters[new_index]
        split += 1
    print(new_str)

"""
C:\code\python_for_physicists\תרגיל 7>q1.py
Please enter key: Python
Please enter note: This exercise is really FUN
For encryption enter 1, for decryption enter 2: 1
HFBznrlCKjwFTxBznETyEsMmtrf

C:\code\python_for_physicists\תרגיל 7>q1.py
Please enter key: Python
Please enter note: HFBznrlCKjwFTxBznETyEsMmtrf
For encryption enter 1, for decryption enter 2: 2
This exercise is really FUN
"""
