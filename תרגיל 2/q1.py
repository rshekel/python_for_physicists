st = input("Please enter a string of length 20 or more: ")
assert len(st) >= 20, "String too short!" 
new_st = st.replace('a', '@#$')
new_st = new_st.replace('A', 'a')
new_st = new_st.replace('@#$', 'A')
print(st + "\n" + new_st)
not_a_count = len(st.replace('a', '').replace('A', ''))
print("Amount of letters that are not 'a' or 'A':", not_a_count)
a_count = st.count('a') + st.count('A')
percentage = a_count / len(st) 
print("There is a", round(percentage, 2), "Percentage of the letter A in your string") 

"""
Input: 
Some things in life are bad They can really make you mad Other things just make you swear and curse When you're chewing on life's gristle Don't grumble, give a whistle And this'll help things turn out for the best And Always look on the bright side Of life Always look on the light side Of life

Output: 
Some things in life are bad They can really make you mad Other things just make you swear and curse When you're chewing on life's gristle Don't grumble, give a whistle And this'll help things turn out for the best And Always look on the bright side Of life Always look on the light side Of life
Some things in life Are bAd They cAn reAlly mAke you mAd Other things just mAke you sweAr And curse When you're chewing on life's gristle Don't grumble, give A whistle and this'll help things turn out for the best and alwAys look on the bright side Of life alwAys look on the light side Of life
Amount of letters that are not 'a' or 'A': 278
There is a 0.05 Percentage of the letter A in your string
"""
