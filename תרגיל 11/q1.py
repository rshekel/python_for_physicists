GOOD_GRADE = 85

def get_best_students(in_file, n, out_file):
    f = open(in_file, 'rb')
    lines = f.readlines()
    ok_students = {} # (id, name)  : number of courses with more than 85
    for line in lines:
        sid, name, f_name, course, grade = line.strip().split(b'~')
        if int(grade) >= GOOD_GRADE:
            if (sid, name, f_name) in ok_students:
                ok_students[(sid, name, f_name)] += [course]
            else:
                ok_students[(sid, name, f_name)] = [course]
    f.close()
    
    new_f = open(out_file, 'wb')
    new_f.write(b"id\t\tname\t\tf_name\r\n")
    for stud_info, good_courses in ok_students.items():
        if len(set(good_courses)) > n:
            new_f.write(stud_info[0] + b"\t\t" + stud_info[1] + b"\t\t" + stud_info[2] + b"\r\n")
    new_f.close()


get_best_students(b"C:\\temp\\studsFull.txt", 2, b"C:\\temp\\best_students.txt")

"""
Output file:

id		name		f_name
9283		shay		ronit
3619		mor		sharvit
5719		lev		michal
4540		kor		ronit
7924		may		roni
3720		ben		dani
9720		levi		uri
4032		raz		tom
3421		mor		vered
4503		kor		may
7019		mor		may
"""