l = eval(input("Please enter a list of things as a pythonic expression: "))
sum = 0
for thing in l:
    if isinstance(thing, (int, float)):
        sum += thing 
    elif isinstance(thing, (range, list, tuple)):
        for subthing in thing:
            if isinstance(subthing, (int, float)):
                sum += subthing

print("The sum of numbers inside lists, ranges and tuples is:", sum)

"""
Input: [23,56,'moshe',True,range(2,7),[34,34.5,41],12.6]
Output: The sum of numbers inside lists, ranges and tuples is: 222.1
"""
