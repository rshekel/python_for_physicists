import random 
min = 1
max = 100

secret = random.randint(min, max)
print("You need to find the number:", secret)

count = 1

while True:
    guess = random.randint(min, max)
    print("guess no.", count, "- my guess is", guess)
    if guess == secret:
        print("Your number really is %s!" % guess)
        break 
    elif guess < secret:
        min = guess + 1 
    elif guess > secret:
        max = guess - 1
    count += 1 

"""
Example output:
You need to find the number: 81
guess no. 1 - my guess is 66
guess no. 2 - my guess is 72
guess no. 3 - my guess is 96
guess no. 4 - my guess is 82
guess no. 5 - my guess is 79
guess no. 6 - my guess is 80
guess no. 7 - my guess is 81
Your number really is 81!
"""
