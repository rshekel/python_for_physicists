import math 
a = float(input('Please enter A value: '))
b = float(input('Please enter B value: '))
c = float(input('Please enter c value: '))

determinant = math.sqrt(b**2 - (4 * a * c))
x1 = (-b + determinant) / (2 * a)
x2 = (-b - determinant) / (2 * a)


print('X1 = ', x1)
print('X2 = ', x2)

"""
Input: 
4, 8, 1.75 

Output:
X1 =  -0.25
X2 =  -1.75
"""
