import math 

r = float(input('Please enter Radius: '))
ball_volume = (4/3) * math.pi * (r ** 3)
big_diagonal = 2 * r 

# a :: small diagonal :: big diagonal === 1 :: sqrt(2) :: sqrt(3) 
a = big_diagonal / math.sqrt(3) 
cube_vloume = a ** 3 

print('Difference between ball volume and cube volume is:', ball_volume - cube_vloume) 

"""
Input: 2.5
Output: Difference between ball volume and cube volume is: 41.39358573355294
"""
