cels = float(input('Please enter temperature in Celsius: '))
far = (1.8 * cels) + 32.0
print('This temperature is equivalent to', far, 'in Fahrenheit')

"""
Input: 100 
Output: This temperature is equivalent to 212.0 in Fahrenheit

Input: 0
Output: This temperature is equivalent to 32.0 in Fahrenheit
"""
